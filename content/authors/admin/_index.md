---
# Display name
name: Ghislene Adjaoute

# Username (this should match the folder name)
authors:
- admin

# Is this the primary user of the site?
superuser: true

# Role/position
role: Data Enthusiast | Engineer | Business Analyst

# Organizations/Affiliations
organizations:
- name: University of California San Diego, Rady School of Management
  url: ""

# Short bio (displayed in user profile at end of posts)
bio: I have over 4+ years of work experience in the healthcare and life sciences industry. In my current master’s I have refined my statistical and visualization skills and learned machine learning models to marry with my subject matter expertise in the life sciences. I am looking to leverage my knowledge and experience into a role as a data scientist/analyst.

interests:
-Applied machine learning
-Deep learning
-Recommendation systems
-API Integration
-Data visualization 
-Bioinformatics
-Data Science for Social Good

education:
  courses:
  - course: Master of Business Analytics (MSBA),
    institution: UCSD Rady School of Management, San Diego
    year: 2020
  - course:  Master of Social Welfare (MSW)
    institution: UCLA Luskin School of Public Affairs, Los Angeles     year: 2018
  - course: BSc in Biology
    institution: California Polytechnic State University, San Luis Obispo               
    year: 2013

# Social/Academic Networking
# For available icons, see: https://sourcethemes.com/academic/docs/page-builder/#icons
#   For an email link, use "fas" icon pack, "envelope" icon, and a link in the
#   form "mailto:your-email@example.com" or "#contact" for contact widget.
social:
- icon: envelope
  icon_pack: fas
  link: '#contact'  # For a direct email link, use "mailto:ghislene.adjaoute@rady.ucsd.edu".
- icon: twitter
  icon_pack: fab
  link: https://twitter.com/gadjaoute
- icon: google-scholar
  icon_pack: ai
  link: https://scholar.google.co.uk/citations?user=sIwtMXoAAAAJ
- icon: github
  icon_pack: fab
  link: https://github.com/gadjaoute
# Link to a PDF of your resume/CV from the About widget.
# To enable, copy your resume/CV to `static/files/cv.pdf` and uncomment the lines below.
# - icon: cv
#   icon_pack: ai
#   link: files/cv.pdf

# Enter email to display Gravatar (if Gravatar enabled in Config)
email: ""

# Organizational groups that you belong to (for People widget)
#   Set this to `[]` or comment out if you are not using People widget.
user_groups:
- Researchers
- Visitors
---

I have over 4+ years of work experience in the healthcare and life sciences industry. In my current master’s I have refined my statistical and visualization skills and learned machine learning models to marry with my subject matter expertise in the life sciences. I am looking to leverage my knowledge and experience into a role as a data scientist/analyst.


Lorem ipsum dolor sit amet, consectetur adipiscing elit. Sed neque elit, tristique placerat feugiat ac, facilisis vitae arcu. Proin eget egestas augue. Praesent ut sem nec arcu pellentesque aliquet. Duis dapibus diam vel metus tempus vulputate.
